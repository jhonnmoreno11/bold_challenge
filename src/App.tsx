import "./App.scss";
import Header from "./components/Header/Header";
import Routes from "./components/pages/Routes";
import { useNavigate } from "react-router-dom";

export default () => {
  const navigate = useNavigate();
  return (
    <div className="app">
      <Header navigate={navigate} />
      <Routes />
    </div>
  );
};
