import { ITransaction } from "../types/transaction.type";

const dateNow = new Date();
const todayInitDay = new Date(
  dateNow.getFullYear(),
  dateNow.getMonth(),
  dateNow.getDate()
);
const todayEndDay = new Date(
  dateNow.getFullYear(),
  dateNow.getMonth(),
  dateNow.getDate(),
  23,
  59,
  59
);

export const currencyFormat = (num: number) => {
  const formatter = new Intl.NumberFormat("en-US", {
    style: "currency",
    currency: "USD",
  });

  return formatter.format(num);
};

export const getMonthName = (date: Date) => {
  const monthNames = [
    "Enero",
    "Febrero",
    "Marzo",
    "Abril",
    "Mayo",
    "Junio",
    "Julio",
    "Agosto",
    "Septiembre",
    "Octubre",
    "Noviembre",
    "Diciembre",
  ];
  return monthNames[date.getMonth()];
};

export const filterByDay = (filterByDate: ITransaction[]) => {
  return filterByDate.filter(
    (transaction) =>
      new Date(transaction.date) >= todayInitDay &&
      new Date(transaction.date) <= todayEndDay
  );
};

export const filterByWeek = (filterByDate: ITransaction[]) => {
  const dayMiliseg = 86400000 * 7;
  const sevenDaysPast = new Date(dateNow.getTime() - dayMiliseg);
  return filterByDate.filter(
    (transaction) =>
      new Date(transaction.date) >= sevenDaysPast &&
      new Date(transaction.date) <= todayEndDay
  );
};

export const filterByMonth = (filterByDate: ITransaction[]) => {
  const firstDayMonth = new Date(
    dateNow.getFullYear() - 1,
    dateNow.getMonth() + 1
  );
  const lastDayMonth = new Date(
    dateNow.getFullYear(),
    dateNow.getMonth() + 1,
    0,
    23,
    59,
    59
  );
  return filterByDate.filter(
    (transaction) =>
      new Date(transaction.date) >= firstDayMonth &&
      new Date(transaction.date) <= lastDayMonth
  );
};
