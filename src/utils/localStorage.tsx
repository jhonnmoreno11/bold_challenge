export const setStorageValue = (value: any) => {
  const validValue = typeof value === "object" ? JSON.stringify(value) : value;
  localStorage.setItem("filter", validValue);
};

export const getStorageValue = (key: string) => {
  return localStorage.getItem(key) || "{}";
};
