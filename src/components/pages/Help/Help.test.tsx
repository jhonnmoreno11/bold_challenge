import "jsdom-global/register";
import { render } from "@testing-library/react";
import Help from "./Help";

describe("Help Component test", () => {
  it("Help defined", () => {
    const { container } = render(<Help />);
    expect(container).toBeDefined();
  });
});
