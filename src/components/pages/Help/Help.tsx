import boldLogoBlue from "../../../assets/icons/bold_logo_blue.svg";
import "./Help.scss";

export default () => {
  return (
    <div className="help__container">
      <img src={boldLogoBlue} alt="" />
      <p className="help__container-contact">
        Contacta a tu asesor Bold de Confianza
      </p>
    </div>
  );
};
