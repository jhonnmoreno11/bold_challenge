import { Navigate, Route, Routes } from "react-router-dom";
import Transaction from "../Transactions/Transaction";
import Help from "./Help/Help";

export default () => {
  return (
    <Routes>
      <Route path="/my-business" element={<Transaction />} />
      <Route path="/help" element={<Help />} />
      <Route path="*" element={<Navigate to="/my-business"></Navigate>} />
    </Routes>
  );
};
