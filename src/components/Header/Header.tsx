import "./Header.scss";
import boldLogo from "../../assets/icons/bold_logo.svg";
import helpIcon from "../../assets/icons/help.svg";

export default (props: IHeader) => {
  const { navigate } = props;

  return (
    <header className="header">
      <img className="bold_logo" src={boldLogo} alt="" />
      <div className="header_options">
        <p onClick={() => navigate("/my-business")}>Mi negocio</p>
        <p onClick={() => navigate("/help")}>
          Ayuda <img className="help__icon" src={helpIcon} alt="" />
        </p>
      </div>
    </header>
  );
};

interface IHeader {
  navigate: (redirecto: string) => void;
}
