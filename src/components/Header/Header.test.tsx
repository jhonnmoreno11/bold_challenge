import "jsdom-global/register";
import { fireEvent, getByText, render } from "@testing-library/react";
import Header from "./Header";

describe("Header Component test", () => {
  const props = {
    navigate: jest.fn(),
  };

  it("Header defined", () => {
    const { container } = render(<Header {...props} />);
    expect(container).toBeDefined();
  });

  it("Header has 2 navigate tabs", () => {
    const { container } = render(<Header {...props} />);
    expect(container.getElementsByTagName("p").length).toEqual(2);
  });

  it("Header go to My business page", () => {
    const { container } = render(<Header {...props} />);
    const myBusinessTag = getByText(container, "Mi negocio");
    fireEvent.click(myBusinessTag);

    expect(props.navigate).toHaveBeenCalledTimes(1);
  });

  it("Header go to Help page", () => {
    const { container } = render(<Header {...props} />);
    const helpTag = getByText(container, "Ayuda");
    fireEvent.click(helpTag);

    expect(props.navigate).toHaveBeenCalledTimes(2);
  });
});
