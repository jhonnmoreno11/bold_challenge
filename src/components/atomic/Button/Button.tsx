import "./Button.scss";

export default (props: IButton) => {
  const { title, onClick, disable } = props;
  return (
    <button disabled={disable || false} className="button" onClick={onClick}>
      {title}
    </button>
  );
};

interface IButton {
  title: string;
  onClick: () => void;
  disable?: boolean;
}
