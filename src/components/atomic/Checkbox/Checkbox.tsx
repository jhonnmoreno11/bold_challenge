import "./Checkbox.scss";

export default (props: ICheckbox) => {
  const { title, value, checked, onChange } = props;
  return (
    <div className="checkbox__container">
      <input
        type="checkbox"
        checked={checked}
        onChange={() => onChange(value)}
      />
      <label htmlFor="">{title}</label>
    </div>
  );
};

interface ICheckbox {
  title: string;
  value: string;
  checked: boolean;
  onChange: (value: string) => void;
}
