import "jsdom-global/register";
import { fireEvent, getByText, render } from "@testing-library/react";
import Button from "../Button/Button";

describe("Button Component test", () => {
  const props = {
    title: "Aceptar",
    onClick: jest.fn(),
  };
  it("Button defined", () => {
    const { container } = render(<Button {...props} />);
    expect(container).toBeDefined();
  });

  it("Button use onClick prop", () => {
    const { container } = render(<Button {...props} />);
    fireEvent(
      getByText(container, "Aceptar"),
      new MouseEvent("click", {
        bubbles: true,
        cancelable: true,
      })
    );
    expect(props.onClick).toHaveBeenCalled();
  });
});
