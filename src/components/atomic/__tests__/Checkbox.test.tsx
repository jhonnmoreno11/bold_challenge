import "jsdom-global/register";
import { fireEvent, render } from "@testing-library/react";
import Checkbox from "../Checkbox/Checkbox";

describe("Checkbox Component test", () => {
  const props = {
    title: "Pagos con datáfono",
    value: "datafono",
    checked: false,
    onChange: jest.fn(),
  };
  it("Checkbox defined", () => {
    const { container } = render(<Checkbox {...props} />);
    expect(container).toBeDefined();
  });

  it("Checkbox use onChange prop", () => {
    const { container } = render(<Checkbox {...props} />);
    const checkbox = container.getElementsByTagName("input")[0];
    fireEvent.click(checkbox);
    expect(props.onChange).toHaveBeenCalled();
  });
});
