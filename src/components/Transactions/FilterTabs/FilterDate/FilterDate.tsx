import { useEffect, useState } from "react";
import {
  filterByDay,
  filterByWeek,
  filterByMonth,
  getMonthName,
} from "../../../../utils/utility";
import {
  getStorageValue,
  setStorageValue,
} from "../../../../utils/localStorage";
import { ITransaction } from "../../../../types/transaction.type";

export default (props: IFilterDate) => {
  const { dataTransactions, setDataFiltered, filterDate } = props;
  const [tab, setTab] = useState<string>("");

  useEffect(() => {
    if (filterDate) {
      const filterLocalStorage = JSON.parse(getStorageValue("filter"));
      filterDays(filterLocalStorage.filterValue);
    }
  }, [filterDate]);

  const filterDays = (newTab: string) => {
    const filter = {
      filterType: "date",
      filterValue: newTab,
    };
    setStorageValue(filter);
    setTab(newTab);
    let filterByDate = [...dataTransactions];

    if (newTab === "month") {
      filterByDate = filterByMonth(filterByDate);
    } else if (newTab === "week") {
      filterByDate = filterByWeek(filterByDate);
    } else {
      filterByDate = filterByDay(filterByDate);
    }
    setDataFiltered(filterByDate);
  };

  return (
    <div className="filter__tabs-options__tab">
      <span
        className={tab === "today" ? "active" : ""}
        onClick={() => filterDays("today")}
      >
        Hoy
      </span>
      <span
        className={tab === "week" ? "active" : ""}
        onClick={() => filterDays("week")}
      >
        Esta semana
      </span>
      <span
        className={tab === "month" ? "active" : ""}
        onClick={() => filterDays("month")}
      >
        {getMonthName(new Date())}
      </span>
    </div>
  );
};
interface IFilterDate {
  dataTransactions: ITransaction[];
  setDataFiltered: (data: ITransaction[]) => void;
  filterDate: boolean;
}
