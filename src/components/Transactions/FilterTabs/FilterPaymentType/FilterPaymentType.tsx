import { useEffect, useRef, useState } from "react";
import "./FilterPaymentType.scss";
import settings from "../../../../assets/icons/settings.svg";
import closeIcon from "../../../../assets/icons/close.svg";
import Checkbox from "../../../atomic/Checkbox/Checkbox";
import {
  getStorageValue,
  setStorageValue,
} from "../../../../utils/localStorage";
import Button from "../../../atomic/Button/Button";
import { ITransaction } from "../../../../types/transaction.type";
import useOnClickOutside from "../../../../utils/useOnClickOutside";

export default (props: IFilter) => {
  const { dataTransactions, setDataFiltered, filterPaymentType } = props;

  const [filter, setFilter] = useState<boolean>(false);
  const [optionsSelected, setOptionsSelected] = useState<string[]>([]);
  const dropdownRef = useRef(null);
  const handleClickOutside = () => {
    setFilter(false);
  };
  useOnClickOutside(dropdownRef, handleClickOutside);

  const filterOptions = [
    {
      title: "Cobro con datáfono",
      value: "datafono",
      onChange: (isSelected: boolean, value: string) =>
        toggleSelected(isSelected, value),
    },
    {
      title: "Cobro con link de pagos",
      value: "link",
      onChange: (isSelected: boolean, value: string) =>
        toggleSelected(isSelected, value),
    },
    {
      title: "Ver todos",
      value: "all",
      onChange: (isSelected: boolean, value: string) =>
        toggleSelected(isSelected, value),
    },
  ];

  useEffect(() => {
    if (filterPaymentType) {
      const filterLocalStorage = JSON.parse(getStorageValue("filter"));
      setOptionsSelected(filterLocalStorage.filterValue);
      filterTransactions(filterLocalStorage.filterValue);
      setFilter(true);
    }
  }, [filterPaymentType]);

  const toggleSelected = (isSelected: boolean, value: string) => {
    let newSelected = [...optionsSelected];
    if (isSelected) {
      newSelected = optionsSelected.filter((item) => item !== value);
    } else {
      if (value === "all") {
        newSelected = ["all"];
      } else {
        newSelected.push(value);
        newSelected = newSelected.filter((item) => item !== "all");
      }
    }
    setOptionsSelected(newSelected);
  };

  const isSelected = (value: string) => {
    return optionsSelected.filter((item) => item === value).length === 1;
  };

  const filterTransactions = (options?: string[]) => {
    const optionsArray = options?.length ? options : optionsSelected;
    const filter = {
      filterType: "paymentType",
      filterValue: optionsArray,
    };
    setStorageValue(filter);
    setFilter(false);

    const findAll = optionsArray.find((item) => item === "all");
    if (findAll) {
      setDataFiltered(dataTransactions);
    } else {
      let newData: ITransaction[] = [];
      optionsArray.forEach((option) => {
        newData = [
          ...newData,
          ...dataTransactions.filter(
            (transaction) => transaction.paymentMethod.boldType === option
          ),
        ];
      });
      setDataFiltered(newData);
    }
  };

  return (
    <div className="filter" ref={dropdownRef}>
      <div
        className={
          filter
            ? "filter__container filter__container-active"
            : "filter__container"
        }
      >
        <div
          className="filter__container-header"
          onClick={() => setFilter(!filter)}
        >
          <p className="filter__container-header__title">FILTRAR</p>
          <img
            className={filter ? "close__icon" : ""}
            src={filter ? closeIcon : settings}
            alt=""
          />
        </div>
        {filter && (
          <div>
            {filterOptions.map((option, index) => (
              <Checkbox
                key={`item-${index}`}
                {...option}
                checked={isSelected(option.value)}
                onChange={(value: string) =>
                  option.onChange(isSelected(option.value), value)
                }
              />
            ))}

            <Button
              title="Aplicar"
              onClick={filterTransactions}
              disable={optionsSelected.length === 0}
            />
          </div>
        )}
      </div>
    </div>
  );
};

interface IFilter {
  dataTransactions: ITransaction[];
  setDataFiltered: (data: ITransaction[]) => void;
  filterPaymentType: boolean;
}
