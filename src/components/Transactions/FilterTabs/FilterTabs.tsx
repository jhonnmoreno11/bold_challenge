import { useEffect, useState } from "react";
import { ITransaction } from "../../../types/transaction.type";
import "./FilterTabs.scss";
import info from "../../../assets/icons/info.svg";
import {
  currencyFormat,
  getMonthName,
  filterByMonth,
} from "../../../utils/utility";
import { getStorageValue, setStorageValue } from "../../../utils/localStorage";
import FilterDate from "./FilterDate/FilterDate";
import FilterPaymentType from "./FilterPaymentType/FilterPaymentType";

export default (props: IFilterTabs) => {
  const { dataTransactions, setDataFiltered } = props;
  const [filterDate, setFilterDate] = useState<boolean>(false);
  const [filterPaymentType, setFilterPaymentType] = useState<boolean>(false);

  useEffect(() => {
    const filterLocalStorage = JSON.parse(getStorageValue("filter"));

    if (dataTransactions.length) {
      if (Object.keys(filterLocalStorage).length > 0) {
        if (filterLocalStorage.filterType === "date") {
          setFilterDate(true);
        } else if (filterLocalStorage.filterType === "paymentType") {
          setFilterPaymentType(true);
        }
      } else {
        const filter = {
          filterType: "date",
          filterValue: "month",
        };
        setStorageValue(filter);
        setFilterDate(true);
      }
    }
  }, [dataTransactions]);

  const getTotalToday = (): any => {
    let filterByDate = [...dataTransactions];

    if (filterByDate.length > 0) {
      filterByDate = filterByMonth(filterByDate);

      const arrayAmounts = filterByDate.map((item) => item.amount);
      if (arrayAmounts.length > 0) {
        return arrayAmounts.reduce(
          (accumulator, currentValue) => accumulator + currentValue
        );
      }
    }
    return 0;
  };

  return (
    <section className="filter__tabs">
      <div className="filter__tabs-container">
        <p className="filter__tabs-container-title">
          Total de ventas de {getMonthName(new Date())}
          <img src={info} alt="" />
        </p>
        <div className="filter__tabs-container-total">
          <p className="filter__tabs-container-total__amount">
            {currencyFormat(getTotalToday())}
          </p>
          <p className="filter__tabs-container-total__date">
            {getMonthName(new Date())}, {new Date().getFullYear()}
          </p>
        </div>
      </div>

      <div className="filter__tabs-options">
        <FilterDate
          dataTransactions={dataTransactions}
          setDataFiltered={setDataFiltered}
          filterDate={filterDate}
        />

        <FilterPaymentType
          dataTransactions={dataTransactions}
          setDataFiltered={setDataFiltered}
          filterPaymentType={filterPaymentType}
        />
      </div>
    </section>
  );
};

interface IFilterTabs {
  dataTransactions: ITransaction[];
  setDataFiltered: (data: ITransaction[]) => void;
}
