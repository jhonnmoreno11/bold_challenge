import { useEffect, useState } from "react";
import { getTransactions } from "../../services/transaction.service";
import { ITransaction } from "../../types/transaction.type";
import FilterTabs from "./FilterTabs/FilterTabs";
import TableTransactions from "./TableTransactions/TableTransactions";
import "./Transaction.scss";

export default () => {
  const [dataTransactions, setDataTransations] = useState<ITransaction[]>([]);
  const [dataFiltered, setDataFiltered] = useState<ITransaction[]>([]);

  useEffect(() => {
    return () => {
      getData();
    };
  }, []);

  const getData = async () => {
    const { data } = await getTransactions();
    setDataTransations(data.transactions);
    setDataFiltered(data.transactions);
  };

  return (
    <div className="transaction__container">
      <FilterTabs
        dataTransactions={dataTransactions}
        setDataFiltered={setDataFiltered}
      />
      <TableTransactions dataFiltered={dataFiltered} />
    </div>
  );
};
