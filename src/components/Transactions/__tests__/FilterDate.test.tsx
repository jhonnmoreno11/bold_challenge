import "jsdom-global/register";
import { fireEvent, getByText, render } from "@testing-library/react";
import FilterDate from "../FilterTabs/FilterDate/FilterDate";
import LocalStorageMock from "../../../../__mocks__/setupFile";
import { getMonthName } from "../../../utils/utility";

describe("FilterDate Component test", () => {
  const props = {
    dataTransactions: [
      {
        state: "success",
        date: "Sat Dec 17 2022 22:50:20 GMT-0500 (hora estándar de Colombia)",
        paymentMethod: {
          type: "pse",
          detail: "CUS 56789",
          boldType: "link",
          logo: "pse",
        },
        transactionID: "47896",
        amount: 58900,
      },
      {
        state: "pending",
        date: "Wed Dec 14 2022 09:46:50 GMT-0500 (hora estándar de Colombia)",
        paymentMethod: {
          type: "card",
          detail: "**** **** **** 4578",
          boldType: "datafono",
          logo: "visa",
        },
        transactionID: "998763",
        amount: 47000,
      },
      {
        state: "failed",
        date: "Sun Dec 11 2022 18:03:47 GMT-0500 (hora estándar de Colombia)",
        paymentMethod: {
          type: "card",
          detail: "**** **** **** 1029",
          boldType: "link",
          logo: "master",
        },
        transactionID: "146320",
        amount: 29300,
      },
      {
        state: "",
        date: "",
        paymentMethod: {
          type: "",
          detail: "",
          boldType: "",
          logo: "",
        },
        transactionID: "",
        amount: 0,
      },
    ],
    setDataFiltered: jest.fn(),
    filterDate: false,
  };
  global.localStorage = new LocalStorageMock();

  it("FilterDate defined", () => {
    const filter = {
      filterType: "date",
      filterValue: "week",
    };
    localStorage.setItem("filter", JSON.stringify(filter));
    const { container } = render(<FilterDate {...props} />);
    expect(container).toBeDefined();
  });

  it("FilterDate tab week active", () => {
    props.filterDate = true;
    const { container } = render(<FilterDate {...props} />);
    const tab = getByText(container, "Esta semana");
    fireEvent.click(tab);

    expect(tab.className).toEqual("active");
  });

  it("FilterDate tab month active", () => {
    const { container } = render(<FilterDate {...props} />);
    const tab = getByText(container, getMonthName(new Date()));
    fireEvent.click(tab);

    expect(tab.className).toEqual("active");
  });

  it("FilterDate tab today active", () => {
    const { container } = render(<FilterDate {...props} />);
    const tab = getByText(container, "Hoy");
    fireEvent.click(tab);

    expect(tab.className).toEqual("active");
  });
});
