import "jsdom-global/register";
import { render } from "@testing-library/react";
import Transaction from "../Transaction";
import LocalStorageMock from "../../../../__mocks__/setupFile";

jest.mock("../../../services/transaction.service", () => {
  const response = { data: { transactions: [] } };
  return {
    getTransactions: jest.fn(() => Promise.resolve(response)),
  };
});

describe("Transaction Component test", () => {
  global.localStorage = new LocalStorageMock();

  it("Transaction have to be defined", () => {
    const { container } = render(<Transaction />);
    expect(container).toBeDefined();
  });
});
