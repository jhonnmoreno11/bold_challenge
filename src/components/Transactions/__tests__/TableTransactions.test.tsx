import "jsdom-global/register";
import { render } from "@testing-library/react";
import TableTransactions from "../TableTransactions/TableTransactions";

describe("TableTransactions Component test", () => {
  const props = {
    dataFiltered: [
      {
        state: "success",
        date: "Sat Dec 17 2022 22:50:20 GMT-0500 (hora estándar de Colombia)",
        paymentMethod: {
          type: "pse",
          detail: "CUS 56789",
          boldType: "link",
          logo: "pse",
        },
        transactionID: "47896",
        amount: 58900,
      },
      {
        state: "pending",
        date: "Wed Dec 14 2022 09:46:50 GMT-0500 (hora estándar de Colombia)",
        paymentMethod: {
          type: "card",
          detail: "**** **** **** 4578",
          boldType: "datafono",
          logo: "visa",
        },
        transactionID: "998763",
        amount: 47000,
      },
      {
        state: "failed",
        date: "Sun Dec 11 2022 18:03:47 GMT-0500 (hora estándar de Colombia)",
        paymentMethod: {
          type: "card",
          detail: "**** **** **** 1029",
          boldType: "link",
          logo: "master",
        },
        transactionID: "146320",
        amount: 29300,
      },
      {
        state: "",
        date: "",
        paymentMethod: {
          type: "",
          detail: "",
          boldType: "",
          logo: "",
        },
        transactionID: "",
        amount: 0,
      },
    ],
  };

  it("TableTransactions defined", () => {
    const { container } = render(<TableTransactions {...props} />);
    expect(container).toBeDefined();
  });

  it("TableTransactions table transactions must be 4", () => {
    const { container } = render(<TableTransactions {...props} />);
    expect(
      container.getElementsByClassName("table__body-columns").length
    ).toEqual(4);
  });
});
