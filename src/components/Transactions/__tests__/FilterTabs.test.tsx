import "jsdom-global/register";
import { render } from "@testing-library/react";
import FilterTabs from "../FilterTabs/FilterTabs";
import LocalStorageMock from "../../../../__mocks__/setupFile";
import { getMonthName } from "../../../utils/utility";

describe("FilterTabs Component test", () => {
  const props = {
    dataTransactions: [
      {
        state: "success",
        date: "Sat Dec 17 2022 22:50:20 GMT-0500 (hora estándar de Colombia)",
        paymentMethod: {
          type: "pse",
          detail: "CUS 56789",
          boldType: "link",
          logo: "pse",
        },
        transactionID: "47896",
        amount: 58900,
      },
      {
        state: "pending",
        date: "Wed Dec 14 2022 09:46:50 GMT-0500 (hora estándar de Colombia)",
        paymentMethod: {
          type: "card",
          detail: "**** **** **** 4578",
          boldType: "datafono",
          logo: "visa",
        },
        transactionID: "998763",
        amount: 47000,
      },
      {
        state: "failed",
        date: "Sun Dec 11 2022 18:03:47 GMT-0500 (hora estándar de Colombia)",
        paymentMethod: {
          type: "card",
          detail: "**** **** **** 1029",
          boldType: "link",
          logo: "master",
        },
        transactionID: "146320",
        amount: 29300,
      },
      {
        state: "",
        date: "",
        paymentMethod: {
          type: "",
          detail: "",
          boldType: "",
          logo: "",
        },
        transactionID: "",
        amount: 0,
      },
    ],
    setDataFiltered: jest.fn(),
  };
  global.localStorage = new LocalStorageMock();

  it("FilterTabs defined", () => {
    const { container } = render(<FilterTabs {...props} />);
    expect(container).toBeDefined();
  });

  it("FilterTabs validate total amount", () => {
    const filter = {
      filterType: "date",
      filterValue: "month",
    };
    localStorage.setItem("filter", JSON.stringify(filter));
    const { container } = render(<FilterTabs {...props} />);
    expect(container.textContent).toContain("$135,200.00");
  });

  it("FilterTabs validate total selling", () => {
    const filter = {
      filterType: "paymentType",
      filterValue: ["datafono"],
    };
    localStorage.setItem("filter", JSON.stringify(filter));
    const { container } = render(<FilterTabs {...props} />);
    expect(container.textContent).toContain(
      `Total de ventas de ${getMonthName(new Date())}`
    );
  });
});
