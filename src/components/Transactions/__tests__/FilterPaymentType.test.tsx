import "jsdom-global/register";
import {
  fireEvent,
  render,
} from "@testing-library/react";
import FilterPaymentType from "../FilterTabs/FilterPaymentType/FilterPaymentType";
import LocalStorageMock from "../../../../__mocks__/setupFile";

describe("FilterPaymentType Component test", () => {
  const props = {
    dataTransactions: [
      {
        state: "success",
        date: "Sat Dec 17 2022 22:50:20 GMT-0500 (hora estándar de Colombia)",
        paymentMethod: {
          type: "pse",
          detail: "CUS 56789",
          boldType: "link",
          logo: "pse",
        },
        transactionID: "47896",
        amount: 58900,
      },
      {
        state: "pending",
        date: "Wed Dec 14 2022 09:46:50 GMT-0500 (hora estándar de Colombia)",
        paymentMethod: {
          type: "card",
          detail: "**** **** **** 4578",
          boldType: "datafono",
          logo: "visa",
        },
        transactionID: "998763",
        amount: 47000,
      },
      {
        state: "failed",
        date: "Sun Dec 11 2022 18:03:47 GMT-0500 (hora estándar de Colombia)",
        paymentMethod: {
          type: "card",
          detail: "**** **** **** 1029",
          boldType: "link",
          logo: "master",
        },
        transactionID: "146320",
        amount: 29300,
      },
      {
        state: "",
        date: "",
        paymentMethod: {
          type: "",
          detail: "",
          boldType: "",
          logo: "",
        },
        transactionID: "",
        amount: 0,
      },
    ],
    setDataFiltered: jest.fn(),
    filterPaymentType: false,
  };
  global.localStorage = new LocalStorageMock();

  it("FilterPaymentType defined", () => {
    const filter = {
      filterType: "paymentType",
      filterValue: [],
    };
    localStorage.setItem("filter", JSON.stringify(filter));
    const { container } = render(<FilterPaymentType {...props} />);
    const openFilter = container.getElementsByClassName(
      "filter__container-header"
    )[0];
    fireEvent.click(openFilter);
    expect(container).toBeDefined();
  });

  it("FilterPaymentType validate close icon after open filter", () => {
    props.filterPaymentType = true;
    const filter = {
      filterType: "paymentType",
      filterValue: ["all"],
    };
    localStorage.setItem("filter", JSON.stringify(filter));
    const { container } = render(<FilterPaymentType {...props} />);

    const closeImg = container.getElementsByClassName("close__icon");
    expect(closeImg.length).toEqual(1);
  });

  it("FilterPaymentType counting checkboxes", () => {
    const filter = {
      filterType: "paymentType",
      filterValue: ["datafono"],
    };
    localStorage.setItem("filter", JSON.stringify(filter));
    const { container } = render(<FilterPaymentType {...props} />);
    const checkboxes = container.getElementsByClassName("checkbox__container");

    expect(checkboxes.length).toEqual(3);
  });

  it("FilterPaymentType button enable after select link filter option", () => {
    const { container } = render(<FilterPaymentType {...props} />);
    const checkboxes = container.getElementsByTagName("input")[1];
    const button = container.getElementsByTagName("button")[0];

    fireEvent.click(checkboxes);
    fireEvent.click(button);
    expect(button.disabled).toBeFalsy();
  });

  it("FilterPaymentType validate if it's selected Datafono option", () => {
    const filter = {
      filterType: "paymentType",
      filterValue: [],
    };
    localStorage.setItem("filter", JSON.stringify(filter));
    const { container } = render(<FilterPaymentType {...props} />);
    const checkboxDatafono = container.getElementsByTagName("input")[0];

    fireEvent.click(checkboxDatafono);

    expect(checkboxDatafono.checked).toBeTruthy();
  });

  it("FilterPaymentType validate if it's deselected Datafono option", () => {
    const filter = {
      filterType: "paymentType",
      filterValue: ["datafono"],
    };
    localStorage.setItem("filter", JSON.stringify(filter));
    const { container } = render(<FilterPaymentType {...props} />);
    const checkboxDatafono = container.getElementsByTagName("input")[0];
    fireEvent.click(checkboxDatafono);

    expect(checkboxDatafono.checked).toBeFalsy();
  });

  it("FilterPaymentType select all filter option", () => {
    const filter = {
      filterType: "paymentType",
      filterValue: ["datafono"],
    };
    localStorage.setItem("filter", JSON.stringify(filter));
    const { container } = render(<FilterPaymentType {...props} />);
    const checkboxAll = container.getElementsByTagName("input")[2];
    fireEvent.click(checkboxAll);

    expect(checkboxAll.checked).toBeTruthy();
  });
});
