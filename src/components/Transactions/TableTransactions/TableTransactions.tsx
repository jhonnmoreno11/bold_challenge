import { ITransaction } from "../../../types/transaction.type";
import { currencyFormat } from "../../../utils/utility";
import "./TableTransactions.scss";
import boldLink from "../../../assets/icons/bold_link.svg";
import boldDatafono from "../../../assets/icons/bold_datafono.svg";
import visa from "../../../assets/icons/visa.svg";
import mastercard from "../../../assets/icons/mastercard.svg";
import pse from "../../../assets/icons/pse.svg";

export default (props: ITableTransactions) => {
  const { dataFiltered } = props;
  const translateState = (state: string) => {
    switch (state) {
      case "success":
        return "Cobro exitoso";
      case "pending":
        return "Cobro pendiente";
      case "failed":
        return "Cobro no realizado";

      default:
        break;
    }
  };

  const setLogo = (logo: string) => {
    switch (logo) {
      case "visa":
        return visa;
      case "master":
        return mastercard;
      case "pse":
        return pse;

      default:
        break;
    }
  };

  const formatDate = (date: string) => {
    const formatedDate = new Date(date);

    return `${formatedDate.getDate()}/${
      formatedDate.getMonth() + 1
    }/${formatedDate.getFullYear()} - ${formatedDate.getHours()}:${formatedDate.getMinutes()}:${formatedDate.getSeconds()}`;
  };

  return (
    <div className="container__table">
      <table className="table">
        <thead className="table__header">
          <tr>
            <td className="table__header-title" colSpan={5}>
              Tus ventas de Diciembre
            </td>
          </tr>
          <tr className="table__header-columns">
            <td>Transacción</td>
            <td>Fecha y hora</td>
            <td>Método de pago</td>
            <td>ID transacción Bold</td>
            <td>Monto</td>
          </tr>
        </thead>
        <tbody className="table__body">
          {dataFiltered.map((transaction, index) => (
            <tr key={`body-tr-${index}`} className="table__body-columns">
              <td className="td__blue">
                <img
                  src={
                    transaction.paymentMethod.boldType === "link"
                      ? boldLink
                      : boldDatafono
                  }
                  alt=""
                />
                {translateState(transaction.state)}
              </td>
              <td className="td__gray">{formatDate(transaction.date)}</td>
              <td className="td__gray table__body-columns-details">
                <img src={setLogo(transaction.paymentMethod.logo)} alt="" />
                {transaction.paymentMethod.detail}
              </td>
              <td className="td__gray">{transaction.transactionID}</td>
              <td className="td__blue">{currencyFormat(transaction.amount)}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};

interface ITableTransactions {
  dataFiltered: ITransaction[];
}
