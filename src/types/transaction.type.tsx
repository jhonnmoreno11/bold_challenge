export interface ITransaction {
  state: string;
  date: string | any;
  paymentMethod: IPaymentMethod;
  transactionID: string;
  amount: number;
}

interface IPaymentMethod {
  type: string;
  detail: string;
  boldType: string;
  logo: string;
}
