import axios from "axios";

export const getTransactions = () => {
  return axios.get("src/assets/db/transactions.json", {
    headers: {
      "Content-Type": "application/json",
      "Accept": "application/json",
    },
  });
};
