import "jsdom-global/register";
import { render } from "@testing-library/react";
import App from "./App";
import { BrowserRouter as Router } from "react-router-dom";

describe("App Component test", () => {
  it("App have to be defined", () => {
    const { container } = render(
      <Router basename="/">
        <App />
      </Router>
    );
    expect(container).toBeDefined();
  });
});
