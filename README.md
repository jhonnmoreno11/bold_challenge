# Instrucciones para ejecutar la aplicación

Luego de haber clonado el repositorio, será necesario hacer la instalación de dependencias node_modules ejecutando el comando **npm i** dentro del proyecto. Todo el proyecto se encuentra sobre la rama por defecto Master.

Paso seguido se podrán ejecutar 2 comandos principales con un fin distinto cada uno.

- npm run dev: permitirá ejecutar el proyecto de manera local y de esta forma poder ver la aplicación funcionando
- npm run test: ejecutará todos los archivos de prueba de la aplicación y permitirá revisar tanto el estado de las pruebas como la cobertura
